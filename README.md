# Basic formatting

This repository contains the service which provide basic formatting of the given text, namely: 
* remove double spaces 
* put space after punctuation mark 
* capitalize the first letter of new sentence.

Input: text

Output: formatted text

To run the service localy follow the next commands, make sure that conda is install on your computer:

1. Clone repository of the service

2. Go to the servise folder in the terminal

3. Create virtual environment:
```
conda create -n service-env python=3.9.12
```

4. Activate virtual environment:
```
conda activate service-env
```

5. Install requirements:
```
pip install -r ./config/python/requirements.txt
```

6. Go to the src folder
```
cd src
```
7. Run service:
```
python corpus.py
```

8. In the browser you use write down http://localhost:5000/

9. Put text to format to the text fiel and press button "Show formatted text"
