# -*- coding: UTF-8 -*-

from flask import Flask, request, render_template, session
from flask_babel import Babel
import settings
from text_formatter import text_formatter

app = Flask(__name__)
babel = Babel(app)

def get_locale():
    if request.args.get('lang'):
        session['lang'] = request.args.get('lang')
    return session.get('lang', 'be')
babel = Babel(app, locale_selector=get_locale)



# Load default config and override config from an environment variable
app.config.update(dict(
    DEBUG=True,
    SECRET_KEY='sdfsdfsfdgdfg4fsd',
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)


@app.route('/', methods=['GET', 'POST'])
def process():
    output_text = None
    _input = ""
    if request.method == "POST":
        _input = request.form.get("inputText")
        output_text = text_formatter(_input)
    return render_template('index.html', output_text=output_text,
                            _input=_input)
   
if __name__ == '__main__':
    app.run(use_reloader=True, debug=True,
            host=settings.host, port=settings.port)
