import re

def text_formatter(input):
    # Delete multiple spaces
    output = re.sub("\s\s+", " ", input)
    # Delete space before punctuation 
    output = re.sub(r'\s([?.!,"](?:\s|$))', r'\1', output)
    # Space before brackets and dash
    output = re.sub(r"(?<=[^\s])(?=[\(\{\[\—])", r" " , output)
    # Space after punctuation
    output = re.sub(r"(?<=[.!?,:;\)\}\]\"\”\—])(?=[^\s])([A-Za-z])",
                    lambda p: " " + p.group(0), output)
    # Capitalizing the beginning of sentence
    output = re.sub("(^|[.?!])\s*([a-zа-яёа-зй-шы-яЁА-ЗЙ-ШЫІіЎў])", 
                    lambda p: p.group(0).upper(), output)
    return output
